﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5_OscarOrtiz_1267717
{
    class circulo
    {
        private double radio;

        public circulo(double unRadio)
        {
            radio = unRadio;
        }

        private double getPerimetro()
        {
            double P;
            P = (2 * Math.PI * radio);
            return P;
        }

        private double getArea()
        {
            return (Math.PI*Math.Pow(radio,2));
        }

        private double getVolumen()
        {
            return (4 * Math.PI * Math.Pow(radio, 3)) / 3;
        }
        public void calGeo(ref double unPerimetro, ref double unArea, ref double unVolumen)
        {
            unPerimetro = getPerimetro();
            unArea = getArea();
            unVolumen = getVolumen();
        }
    }
}
