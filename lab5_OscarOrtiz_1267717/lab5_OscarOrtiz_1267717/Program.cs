﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5_OscarOrtiz_1267717
{
    class Program
    {
        static void Main(string[] args)
        {
            double unPerimetro=0, unArea=0, unVolumen=0;
            Random  Aleatorio= new Random();
            double R;

            R = Aleatorio.Next(0, 11);

            circulo objcirculo;//declaracion
            Console.WriteLine("Su radio aleatorio es"+R);
            //inicializacion
            objcirculo = new circulo(R);
            objcirculo.calGeo(ref unPerimetro, ref unArea, ref unVolumen);

            Console.WriteLine("El perimetro = "+Math.Round(unPerimetro,2)+"\nArea = "+Math.Round(unArea,2)+"\nvolumen = "+Math.Round(unVolumen,2));
            Console.ReadKey();
        }
    }
}
